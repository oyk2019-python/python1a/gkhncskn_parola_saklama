parolalar = []    #"parola adında bir liste oluşturduk
while True:       #Döngü oluşturduk
    print('Bir işlem seçin')    #parantez içindeki yazıyı printler
    print('1- Parolaları Listele')  #parantez içindeki yazıyı printler
    print('2- Yeni Parola Kaydet')  #parantez içindeki yazıyı printler
    islem = input('Ne Yapmak İstiyorsun :')     #kullanıcıdan işlem tipini seçmesini istiyoruz
    if islem.isdigit():         #rakam olup olmadığını kontorl eder eğer rakamsa işleme devam eder
        islem_int = int(islem)  #"islem_int" değişkenini kullanıcıdan aldığımız "işlem" değişkenine girilen rakamı integer olarak eşitler
        if islem_int not in [1, 2]: # Eğer "işlem_int" değişkeninine içinde 1 veya 2 yoksa if çalışır.
            print('Hatalı işlem girişi') # parantez içindeki yazıyı printler
            continue    #üst satır çalıştığı an kullanıcıdan yeni "işlem" ister
        if islem_int == 2:    #Eğer kullanıcı "işlem_int 'e 2 girerse if çalışır

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')  #kullanıcıdan "girdi_ismi" değişkenini ister

            kullanici_adi = input('Kullanici Adi Girin :')  #kullanıcıdan "kullanici_adi" değişkenini ister

            parola = input('Parola :')  #kullanıcıdan "parola" değişkenini ister
            parola2 = input('Parola Yeniden :') #kullanıcıdan "parola2" değişkenini ister
            eposta = input('Kayitli E-posta :') #kullanıcıdan "eposta" değişkenini ister
            gizlisorucevabi = input('Gizli Soru Cevabı :') #kullanıcıdan "gizlisorucevabi" değişkenini ister
            if kullanici_adi.strip() == '':  #"kullanıcı adı" değişkeni boş ise if çalışır (strip komutu boşlukları siler)
                print('kullanici_adi girmediz')     #parantez içindeki yazıyı printler
                continue    #üst satır çalıştığı an kullanıcıdan yeniden girdileri ister
            if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:
                print('Parolalar eşit değil')
                continue

            yeni_girdi = {   #"yeni girdi adında bir sözlük oluşturduk
                'girdi_ismi': girdi_ismi,   #key, value biçiminde sözlük içeriğini "value" leri kullanıcıdan aldığımız verilerle doldurduk
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi)  #oluşan yeni girdi sözlüğünü parolalar dosyasının içine attık
            continue #üst satır çalıştığı an kullanıcıdan yeniden girdileri ister
        elif islem_int == 1: # kullanıcıdan alınan veri 1 ise elif çalışır
            alt_islem_parola_no = 0 #"alt_islem_parola_no" değişkenini 0'a eşitledik
            for parola in parolalar: #parola değişkenini parolalar içinde gezdirdik
                alt_islem_parola_no += 1  #"alt_islem_parola_no" değişkenini 1 arttırdık
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi'))) #format metodu ile parantez içindeki kullanıcıdan aldığımız verilerle printledik
            alt_islem = input('Yukarıdakilerden hangisi ?: ') #alt_işlem değişkeni oluşturup kullanıcıdan veri istedik
            if alt_islem.isdigit(): # alt işlemi rakam olup olmadığını sorgular
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem): #alt_işlem değişkeninin 1 den küçük olması parolaların içindeki veri sayısının alt_işlem değişkeninden küçük olma şartını sağlarsa ife girer
                    print('Hatalı parola seçimi') #parantez içindeki yazıyı printler
                    continue #üst satır çalıştığı an kullanıcıdan yeniden girdileri ister
                parola = parolalar[int(alt_islem) - 1] #parola değişkenini parolalar listesinin parantez işlem sonucundan aldığı sayıya denk gelen veriye eşitler
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),                                #değişkenleri parola sözlüğünden verilen keylere göre
                    eposta=parola.get('eposta'),                                #Valueleri çeker ve format metodu ile printler
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue                                        #üst satır çalıştığı an kullanıcıdan yeniden girdileri ister

    print('Hatalı giriş yaptınız')      #parantez içindeki yazıyı printler..